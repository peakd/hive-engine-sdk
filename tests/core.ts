import { hiveEngineApi } from '../src/hiveEngineApi'

describe('blockchain', () => {
  test('getTransaction', async () => {
    const trx = await hiveEngineApi.getTransaction('7c97e39202186b1e1c38135b978c459ddd124dd1')
    expect(trx?.hash).toBe('6e38b333a48b3379423abec44c5bae4349df0ad9fdcf3fe851ec312367b53da4')
  })
})

describe('tables', () => {
  test('findOne', async () => {
    const record = await hiveEngineApi.findOne('tokens', 'tokens', { symbol: 'ZING' })
    expect(record).toBeDefined()
  })

  test('findMany', async () => {
    const records = await hiveEngineApi.findMany('marketpools', 'pools', {
      tokenPair: { $in: ['PKM:SPS', 'PXL:SWAP.HIVE'] }
    })
    expect(records.length).toBe(2)
  })
})
