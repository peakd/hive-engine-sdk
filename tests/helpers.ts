import { hiveEngineApi } from '../src/hiveEngineApi'

describe('helpers', () => {
  test('getToken', async () => {
    const record = await hiveEngineApi.getToken('ZING')
    expect(record).toBeDefined()
  })
})
