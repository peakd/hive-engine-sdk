# hive-engine-sdk

The small library to interact with Hive Engine (https://hive-engine.com/) nodes in a reliable way.

**IMPORTANT**: The package is still under development and things may break unexpectedly.

## Installation

```bash
$ npm i @peakd/hive-engine-sdk
```

### Basic usage

```javascript
import { hiveEngineApi } from '@peakd/hive-engine-sdk'

const tokenData = await hiveEngineApi.getToken('BEE')
console.log(tokenData)
```
